
# #this is for a particular hypothesis, that of argocd being admin
# #fake crossplane, ldap
# CREATEDEPS=\
# st/deploy-argocd \
# st/deploy-argocd-admin \
# st/deploy-tekton \
# st/deploy-pull-secret \
# st/deploy-fake-ldap-groups \
# st/deploy-fake-crossplane \
# st/deploy-htpasswd \
# st/apply-argocd-infra \
# st/deploy-external-secret-operator \
# st/deploy-eso-aws-demo \
# st/destroy-bootstrap \
# get-kubeadmin
# #deploy-actual-ldap \

#nsconf kludge hypothesis
CREATEDEPS=\
st/deploy-autoscaler \
st/deploy-argocd \
st/deploy-argocd-admin \
st/ns-conf-kludge \
st/deploy-tekton \
st/deploy-pull-secret \
st/deploy-fake-ldap-groups \
st/deploy-fake-crossplane \
st/deploy-htpasswd \
st/apply-argocd-infra \
st/deploy-external-secret-operator \
st/deploy-eso-aws-demo \
st/destroy-bootstrap \
st/restore-secrets-to-moonstrike \
get-kubeadmin 
#deploy-actual-ldap \
