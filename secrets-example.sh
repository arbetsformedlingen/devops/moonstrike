export SSHKEY=<your public ssh key in text (not path to file), its needed for some reason during install>
export PULLSECRET=<pullsecret from redhat, needed even for okd>
export CLUSTERNAME=<clustername,  moonstrike for instance>

#see the file eso-aws-sm.org for details how to create these secrets
export AWSSM_ACCESS_KEY=<aws sm secret>
export AWSSM_SECRET_KEY=<aws sm key>

export ADMINPASSWD=<a strong pwd, for the defaule "administrator" user>
