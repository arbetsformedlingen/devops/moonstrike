# * Setup
# ** Make configuration
.ONESHELL:

SHELL = /bin/bash

# ** Logging macros

# the directory where state logging files are stored
st:
	mkdir -p st

define st_done
	@msg="***************** Finished $1 at $$(date) *****************"
	@echo $$msg>${1}
	@echo $$msg>>st/log
	@echo $$msg
endef

define st_start
	@msg="***************** Started $1 at $$(date) *****************"
	@msg="Started $1 at $$(date)"
	@echo $$msg>${1}_start
	@echo $$msg>>st/log
	@echo $$msg
	@set -e # so shell exits on errors
endef

REQUIRED_TOOLS = yq oc htpasswd

tst_st_done:
	$(call st_done,$@)

#"kubeadmin", and password is in kubeconfig"
OC=oc --kubeconfig config/auth/kubeconfig
MAXATTEMPT=20

# ** Configuration
include settings.mk
include deps.mk
ifeq ($(CLUSTERTYPE),OKD)
  CLUSTERTYPE=OKD
  OCPINSTALL=./okd-installer/openshift-install
  ARGOCDSUB=argocd-operator-sub.yaml
  ARGOCDDEPLOYMENT=argocd-operator-controller-manager
else
  CLUSTERTYPE=OCP
  OCPINSTALL=./ocp-installer/openshift-install
  ARGOCDSUB=openshift-gitops-sub.yaml
  ARGOCDDEPLOYMENT=gitops-operator-controller-manager
endif

#4.14 needs the folowing, in a 4.13 cluster
# oc --kubeconfig config/auth/kubeconfig -n openshift-config patch cm admin-acks --patch '{"data":{"ack-4.13-kube-1.27-api-removals-in-4.14":"true"}}' --type=merge

# the original hypothesis
#  check-ssh-agent create deploy-argocd deploy-tekton deploy-gitlab deploy-pull-secret deploy-fake-ldap-groups deploy-htpasswd  apply-argocd-infra  deploy-fake-crossplane deploy-nsconf deploy-nsconf-permission-kludge




# echo the settings, that you might tweak
# normally, CLUSTERSIZE, CLUSTERTYPE, CREATEDEPS
echosettings:
	@source secrets.sh
	@echo "Cluster size[single, big]: $(CLUSTERSIZE)"
	@echo "Cluster Type[OKD, OCP]   : $(CLUSTERTYPE)"
	@echo "  Argocd sub             : $(ARGOCDSUB) ($(CLUSTERTYPE))"
	@echo "  Argocd target          : $(ARGOCDDEPLOYMENT) ($(CLUSTERTYPE))"
	@echo "  Installer              : $(OCPINSTALL) ($(CLUSTERTYPE))"
	@echo "Install targets          : $(CREATEDEPS)"
	@echo "Clustername              : $(CLUSTERNAME)"
	@echo "Argocd infra layer       : $(ARGOCDINFRALAYER)"
	@echo "as shell"
	source settings.mk
	@echo "Clustername              : $$CLUSTERNAME"
	@echo "Argocd infra layer       : $$ARGOCDINFRALAYER"

echosecrets:
	source secrets.sh
	@echo "SSHKEY=$$SSHKEY"
	@echo "PULLSECRET=$$PULLSECRET"
	@echo "AWSSM_ACCESS_KEY=$$AWSSM_ACCESS_KEY"
	@echo "AWSSM_SECRET_KEY=$$AWSSM_SECRET_KEY"
	@echo "ADMINPASSWD=$$ADMINPASSWD"
	@echo "LDAPS=$$LDAPS"
	cat pull_secret.json

verifytools:
	for tool in $(REQUIRED_TOOLS); do if ! command -v $$tool &> /dev/null; then echo "**** please install $$tool"; exit 1; fi; done

echotools:
	for tool in $(REQUIRED_TOOLS); do which $$tool; done

echoenv: echosettings echosecrets echotools
	echo verify settings

# ** Preparations

check-ssh-agent:
	@if [ -z "$$SSH_AGENT_PID" ] || ! ps -p $$SSH_AGENT_PID > /dev/null; then echo "**** no ssh agent" >&2; exit 1; fi

check-aws-credentials:
	@if [ ! -f ~/.aws/credentials ]; then echo "**** You need to set up aws credentials! ``" >&2; exit 1; fi
sshagent:
	eval "$(ssh-agent -s)"
	ssh-add ~/.ssh/id_rsa

check-existing-files:
	if [ -d config/terraform ]; then echo "**** please cleanup from previous build first. Try `make clean`" >&2; exit 1; fi

$(OCPINSTALL):
	$(MAKE) download-$(CLUSTERTYPE)-installer

st/preamble: st check-existing-files $(OCPINSTALL)
# * Main targets, start here
st/destroy-bootstrap:
	$(call st_start,$@)
	$(OCPINSTALL) destroy bootstrap --dir=config  --log-level=debug
	$(call st_done,$@)


#edit console icon
#https://access.redhat.com/documentation/en-us/openshift_container_platform/4.7/html/web_console/customizing-web-console

#there needs to be a readiness check for "gitops" before deploying argocd it seems


create-world: verifytools st/preamble check-aws-credentials check-ssh-agent st/create-cluster $(CREATEDEPS) st/destroy-bootstrap
	$(call st_start,$@)
	echo "stuff should be working now (in your dreams...)"
	echo "do 'make destroy-world' when you are done"

destroy-world:
	$(call st_start,$@)
	echo "should delete argocd dir as well, so we get a fresh clone next time"
	$(OCPINSTALL) destroy cluster --dir=config  --log-level=debug
	echo "should also reomve the cluster from admin pages"
	rm -rf st
clean:
	$(call st_start,$@)
	rm -rf config ocp-installer okd-installer st



# * Run the cluster installer
st/create-cluster:
	$(call st_start,$@)
	echo "dont forget need ssh agent running"
	echo "expect at least 30 mins for this..."
	mkdir -p config
	source $$PWD/secrets.sh
	# below is a bit awkward, because clustername is a make variable, but i also need it to be a shell variable, for envsubst
	source $$PWD/settings.mk
	export CLUSTERNAME=$(CLUSTERNAME)
	export AWS_REGION=$(AWS_REGION)
	envsubst < install-config-template-$(CLUSTERSIZE).yaml > config/install-config.yaml
	$(OCPINSTALL) create cluster --dir=config  --log-level=debug
	$(call st_done,$@)

# * Pull secrets, otherwise images cant be pulled
st/deploy-pull-secret:
	$(call st_start,$@)
	$(OC) set data secret/pull-secret -n openshift-config --from-file=.dockerconfigjson=pull_secret.json
	$(call st_done,$@)

pull_secret.json:
	#TODO you need to fetch the secret from the "testing" cluster, this isnt managed atm
	oc  get  secret/pull-secret -n openshift-config  -o jsonpath="{.data.\.dockerconfigjson}" | base64 -d > pull_secret.json

# * Argocd (Gitops operator)
st/deploy-argocd:
	$(call st_start,$@)
	#this is the same for both ocp and okd
	$(OC) apply -f $(ARGOCDSUB)
	for i in {1..$(MAXATTEMPT)}; do \
           echo "Attempt $$i"; \
           $(OC) rollout status deployment/$(ARGOCDDEPLOYMENT) -n openshift-operators && break || sleep 5; \
	done
	echo $(CLUSTERTYPE)
	$(call st_done,$@)
ifeq ($(CLUSTERTYPE),OCP)
	echo "for ocp"
else
	#OKD
	$(OC) create namespace openshift-gitops # because this iis what happens on openshift, but the upstream doesnt do this
	echo "somehow also fix argocd login for users TODO"
	#admin user pwd
	#oc -n openshift-gitops get secret argocd-secret -o jsonpath="{.data.admin\.password}" | base64 -d
	#but you mus also enable the admin user in argocd-cm
endif

st/deploy-argocd-admin:
	$(OC) apply -f argocd-admin.yaml
	$(call st_done,$@)
argocd-infra:
	$(call st_start,$@)
	git clone https://gitlab.com/arbetsformedlingen/devops/argocd-infra.git



st/apply-argocd-infra: argocd-infra
	$(call st_start,$@)
	cd argocd-infra && git pull origin main && cd -
	#ARGOCDINFRALAYER is for example moonstrike, or testing, kustomize overlays in the argocd repo
	$(OC) apply -k argocd-infra/kustomize/clusters/$(ARGOCDINFRALAYER)
	$(call st_done,$@)
download-argocd-cli:
	curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
        # you need to login, and stuff to be able to do things
	# ./argocd-linux-amd64 login openshift-gitops-server-openshift-gitops.apps.jvmoonstrike.jobtechdev.se  --insecure --sso


# * Tekton (Pipelines)
st/deploy-tekton:
	$(call st_start,$@)
	@echo $(CLUSTERTYPE)
ifeq ($(CLUSTERTYPE),OCP)
	$(OC) apply -f openshift-pipelines-operator-sub.yaml
	for i in {1..$(MAXATTEMPT)}; do \
           echo "Attempt $$i"; \
           $(OC) rollout status deployment/openshift-pipelines-operator -n openshift-operators && break || sleep 5; \
	done
else
	#OKD, theres no pipeline operator for okd, use upstream tekton
	$(OC) apply -f https://raw.githubusercontent.com/tektoncd/operator/main/config/crs/kubernetes/config/all/operator_v1alpha1_config_cr.yaml
endif
	$(call st_done,$@)
# * External secrets operator
st/deploy-external-secret-operator:
	$(call st_start,$@)
	#this is the same for both ocp and okd
	$(OC) apply -f external-secrets-sub.yaml
	for i in {1..$(MAXATTEMPT)}; do \
           echo "Attempt $$i"; \
           $(OC) rollout status deployment/external-secrets-operator-controller-manager -n openshift-operators && break || sleep 5; \
	done
	# after the operator installs, create an instance
	$(OC) apply -f external-secret-operatorconfig.yaml
	$(call st_done,$@)

st/deploy-eso-aws-demo:
	source secrets.sh
	echo $$AWSSM_ACCESS_KEY
	echo $$AWSSM_SECRET_KEY
	for try in {1..$(MAXATTEMPT)}; do
	  echo "Attempt #$$try ..."
	  {
	    $(OC) --namespace=openshift-operators create secret generic aws-secret-manager-key --from-literal=access-key=$$AWSSM_ACCESS_KEY --from-literal=secret=$$AWSSM_SECRET_KEY
	    $(OC) apply --namespace=openshift-operators -f aws-eso-demo.yaml
	  } && break || {
	    echo "deploy-eso-aws-demo operation failed, retrying in 5 seconds..."
	    sleep 5
	  }
	done
	$(call st_done,$@)
# * Crossplane
deploy-crossplane: crossplane-infra
	#TODO if i dont deploy crossplane, a bunch of projects cant be deployed, because you need DnsRecord CRD (or maybe this isnt crossplane, but rather the ingress controller?)
	#crossplane.jobtechdev.se/DnsRecord
	#maybe try Upbound Universal Crossplane (UXP), tried it briefly
	#	echo dummy
	#https://gitlab.com/arbetsformedlingen/devops/crossplane/crossplane-infra/-/blob/main/INSTALL.md?ref_type=heads
	#these 2 just deploys the crossplane provders we need, they wont be functioning
	$(OC) create namespace crossplane-system
	$(OC)  apply -f crossplane-infra/kustomize/base/aws-jet-provider.yaml
	$(OC)  apply -f crossplane-infra/kustomize/base/ocp-controllerconfig.yaml
	$(OC)  apply -k crossplane-infra/kustomize/clusters/$(ARGOCDINFRALAYER)  # Change last part to cluster name

st/deploy-fake-crossplane:
	$(OC)  apply -f fake-crossplane2.yaml
	$(call st_done,$@)

crossplane-infra:
	git clone https://gitlab.com/arbetsformedlingen/devops/crossplane/crossplane-infra.git

#stuff that should be done on the fresh clone
# - edit things that would interfere with other clusters, such as external write db:s,
# - crossplane dns must be cluster specific
# - datomic will likely not work, so use datahike
# - edit routes to be cluster specific

# * Jobtech Clusterconfig
deploy-openshift-clusterconfig:
#/home/joakim/jobtech/ocpsinglenode/openshift-main/af-new-onprem-2022/ocp/clusterconfig/README.md
#this is from onprem gitrepo
# aparently ldap needs to be installd so calamari group is installed so nsconf works
	echo "dummy, but should be done fairly early i guess"

# * User management
# ** Fake Ldap groups
st/deploy-fake-ldap-groups:
	$(call st_start,$@)
	$(OC) apply -f fake-ldap-groups.yaml
	$(call st_done,$@)
# ** Install ldap
deploy-actual-ldap:
	$(call st_start,$@)
	# this patches logins so you can login using ldap
	source secrets.sh
	envsubst < oauth-ldap.json > oauth-ldap-2.json
	$(OC) patch OAuth cluster --type=json --patch-file oauth-ldap-2.json

deploy-actual-ldap-sync:
	$(call st_start,$@)
	#this rule is for ldap sync, but its not very robust, because helm
	$(OC) delete clusterrolebinding af-cluster-admins #this is setup somewhere else, but not compatible with this target
	$(OC) create ns af-infra
	$(OC) create ns aardvark # its weird the chart uses this ns, aardvark isnt used anymore
	#this rule seems incompatible with "fake-ldap-groups"
	export KUBECONFIG=`realpath config/auth/kubeconfig`
	@echo $$KUBECONFIG
	cd openshift-main/helm-chart/clusterconfig
	helm upgrade -i clusterconfig . -f values-moonstrike.yaml
	#cronjob version needs to be v1, not v1beta1
	# should rather be helm install clusterconfig . -f values.yaml , where values yaml adjusted
	# cd/home/joakim/jobtech/ocpsinglenode/openshift-main/helm-chart/clusterconfig
	#helm upgrade --install ldapsync . -f values-moonstrike.yaml


# ** Manage htpasswd admin user
administrator-htpasswd-file:
	$(call st_start,$@)
	source secrets.sh
	@echo "pwd: $$ADMINPASSWD"
	htpasswd -cbB administrator-htpasswd-file administrator $$ADMINPASSWD
	#now create a htpasswd idp, here...
	#and the rolebinding


#this creates the administrator user, with the known pwd
# sometimes its not clear how to make the user work in argocd though,
# so that the user can sync apps.
# https://access.redhat.com/solutions/6975821
# oc adm groups add-users cluster-admins administrator
# add the user to group cluster-admins(in the gui, or otherwise)
#
st/deploy-htpasswd:administrator-htpasswd-file
	$(call st_start,$@)
	$(OC) create secret generic administrator-htpasswd-file --from-file=htpasswd=administrator-htpasswd-file -n openshift-config
	$(OC) apply -f administrator-htpasswd-auth.yaml -n openshift-config
	#oc adm groups add-users cluster-admins administrator
	#oc get argocd -n openshift-gitops -o yaml | grep -i rbac -A5
	$(call st_done,$@)
clean-htpasswd:
	$(call st_start,$@)
	rm administrator-htpasswd-file && true
	$(OC) delete secret  administrator-htpasswd-file -n openshift-config && true

# * Nsconf
deploy-nsconf:
	$(call st_start,$@)
	#https://gitlab.com/arbetsformedlingen/devops/ns-conf
	# instead: Create a namespace in the cluster manually and apply the label to the namespace (argocd.argoproj.io/managed-by=openshift-gitops)
	$(OC) new-project ns-conf-$(ARGOCDINFRALAYER)
	$(OC) project ns-conf-$(ARGOCDINFRALAYER)
	$(OC) apply -f  permission.yaml -n ns-conf-$(ARGOCDINFRALAYER)
	$(OC) label namespaces  ns-conf-$(ARGOCDINFRALAYER) argocd.argoproj.io/managed-by=openshift-gitops #with this probably the permission.yaml apply isnt needed, still seems it needed
	@echo "Now argocd needs to sync the ns-conf-$(ARGOCDINFRALAYER) namespace, then ns-conf can begin creating namespaces"

deploy-nsconf-permission-kludge:
	$(call st_start,$@)
	@echo "these should be mechanically derived from nsconf infra"
	#$(OC) apply -f  permission.yaml -n  historical-search-frontend-develop
	# kludge wait for namespaces to be created by nsconf, which might take a while, otherwise we cant kludge the namespace permissions!
	#!/bin/bash
	target=57
	value=$$(oc get namespaces | grep -v openshift | wc -l)
	while [[ "$$value" -eq $$target ]]
	do
	   echo "Current count is $$value. Waiting for it to be at least $$target..."
	   sleep 5 # waits 5 seconds
	   value=$$(oc get namespaces | grep -v openshift | wc -l)
	done
	echo "ns Count is now at least $$target. Exiting loop."

	# Get list of all namespaces, apply perms kludge to each
	namespaces=$$(oc get namespaces -o jsonpath="{.items[*].metadata.name}")
	for namespace in $$namespaces; do
	    # If the namespace doesn't start with "openshift-", apply the permission file.
	    if ! [[ $$namespace =~ ^openshift- ]]; then
	        echo $$namespace
		$(OC) apply -f permission.yaml -n $$namespace
	    fi
	done

# * Download installers
download-OKD-installer:
	$(call st_start,$@)
	test -f $(OCPINSTALL) && { echo "**** installer already exists" >&2; exit 1; }
	mkdir okd-installer
	cd okd-installer
	wget https://github.com/okd-project/okd/releases/download/$(OKD_VERSION)/openshift-install-linux-$(OKD_VERSION).tar.gz
	tar -zxvf *.tar.gz

download-OCP-installer:
	$(call st_start,$@)
	test -f $(OCPINSTALL) && { echo "**** installer already exists" >&2; exit 1; }
	mkdir ocp-installer
	cd ocp-installer
	curl -k https://mirror.openshift.com/pub/openshift-v4/clients/ocp/$(OCP_VERSION)/openshift-install-linux.tar.gz > openshift-install-linux.tar.gz
	tar -zxvf *.tar.gz

# * Work with secrets
dump-secrets-from-$(ARGOCDINFRALAYER):
	@echo "WARNING this is tricky, be logged in to testing and moonstrike at the same time"
	# run this in a working cluster, to understand what secrets are needed
	mkdir -p secrets
	echo "secrets in $(ARGOCDINFRALAYER), there needs to be a context '$(SECRETSFROMCONTEXT)' context for this to work, check oc config get-contexts"
	oc config use-context $(SECRETSFROMCONTEXT)
	@IFS=$$'\n' secrets=($$(oc get secrets --all-namespaces | grep -Ev 'kubernetes.io|crossplane|openshift|kube-system|helm.sh/release.v1|skupper|container-sync|vault|vuln|default|slask|gitlab-express|yrkesinfo'| tail -n +2))
	for secret in "$${secrets[@]}"; do
		#echo $$secret
		@IFS=' ' read -aFIELDS <<< "$$secret"
		echo "ns: $${FIELDS[0]} name: $${FIELDS[0]}"
		oc get secret -n $${FIELDS[0]} $${FIELDS[1]} -o yaml|yq eval 'del(.metadata.annotations, .metadata.uid, .metadata.creationTimestamp, .metadata.resourceVersion)' >$(ARGOCDINFRALAYER)-secrets/$${FIELDS[0]}---$${FIELDS[1]}.yaml
	done
	#switch back to moonstrike. hmm, im not sure this is needed since the $(OC) is used everywehere in this file
	#	oc config use-context $(MOONSTRIKECONTEXT)
st/restore-secrets-to-moonstrike:
	#here we could copy all seccrets to the new cluster, but they might conflict so be careful
	for file in $(ARGOCDINFRALAYER)-secrets/*; do  $(OC) apply -f "$$file"; done

	$(OC) get secrets --all-namespaces | grep -Ev 'kubernetes.io|crossplane|openshift|kube-system|helm.sh/release.v1|skupper'
	$(call st_done,$@)


# ** sealed secrets

kubeseal:
	go install github.com/bitnami-labs/sealed-secrets/cmd/kubeseal@main

# * Get kubeadmin
get-kubeadmin:
	$(call st_start,$@)
	@grep "Access the OpenShift web-console" config/.openshift_install.log |tail -n 1
	@grep "Login to the console with user" config/.openshift_install.log |tail -n 1

# * Verify things
# ** figure out routes, and try curling them to see if stuff seems to work
# curl each route and see if something works
# oc get routes -o jsonpath='{range .items[*]}{"//"}{.spec.tls.termination}{"://"}{.spec.host}{"\n"}{end}' --all-namespaces
megacurlimpl:
	$(call st_start,$@)
	@IFS=$$'\n' urls=($$($(OC) get routes -o jsonpath='{range .items[*]}{"|"}{.spec.tls.termination}{"|"}{.spec.host}{";"}{.metadata.namespace}{"\n"}{end}' --all-namespaces))
	printf "| %-15s | %-120s | %-10s|\n" "testing" "url" "status"
	for urlandns in "$${urls[@]}"; do
	  IFS=';' read -ra ADDR <<< "$${urlandns}"
	  if [[ $$url  =~ "prometheus|openshift" ]] ; then
	      break
	  fi
	  ns=$${ADDR[1]}
	  url=$${ADDR[0]}
	  url="$${url//|edge|/https:\/\/}"
	  url="$${url//||/http:\/\/}"
	  url="$${url//|passthrough|/https:\/\/}"
	  url="$${url//|reencrypt|/https:\/\/}"
	  if [[ $$url == *moonstrike* ]]; then
		 status=$$(curl -L -k -s -o /dev/null -w "%{http_code}" $$url)
		 printf "| %-15s | %-120s | %-10s| %-40s|\n" "testing" $$url $$status $$ns
		#		 echo "testing ........  $$url $$status"
	  else
		 printf "| %-15s | %-120s | %-10s| %-40s|\n" "NOT testing" $$url "-" $$ns
		 #echo "NOT testing ....  $$url is not a cluster specific url"
	  fi
	  #sleep 1
	done

megacurl:
	$(call st_start,$@)
	$(MAKE) megacurlimpl > curlresults.txt
	sort -t'|' -k4,4r curlresults.txt
	echo -n "200: "
	awk -F"|" '$$4 ~ /20/ {sum++} END {print sum}' curlresults.txt
	echo -n  "400: "
	awk -F"|" '$$4 ~ /40/ {sum++} END {print sum}' curlresults.txt
	echo -n  "500: "
	awk -F"|" '$$4 ~ /50/ {sum++} END {print sum}' curlresults.txt
	echo  -n  "-  : "
	awk -F"|" '$$4 ~ /-/ {sum++} END {print sum}' curlresults.txt

# ** try some well known services to validate json
# install schemathesis 1st
#TODO the routes should be figured out rather than being hardcoded as here

install-schemathesis:
	python -m pip install schemathesis

schemathesis: schemathesis-berikning schemathesis-joblinks schemathesis-taxonomy schemathesis-jobsearch

schemathesis-berikning:
	st run   --request-tls-verify=false  --checks all https://enrichments-jobtech-jobad-enrichments-test.apps.$(CLUSTERNAME).jobtechdev.se/swagger.json || true

schemathesis-joblinks:
	st run   --request-tls-verify=false --checks all 	https://joblinks-search-joblinks-search-api-develop.apps.$(CLUSTERNAME).jobtechdev.se/swagger.json || true

schemathesis-taxonomy:
	st run   --request-tls-verify=false --checks all http://api-jobtech-taxonomy-api-testing-read.apps.$(CLUSTERNAME).jobtechdev.se/v1/taxonomy/swagger.json || true

schemathesis-jobsearch:
	st run   --request-tls-verify=false --checks all https://jobsearch-jobsearch-apis-develop.apps.$(CLUSTERNAME).jobtechdev.se/swagger.json || true

# * Clone repos, drive changes locally rather than from argocd
# sometimes its very inconvenient to experiment with things using the gitops aproach
# here we clone nsconf, and argocd infra repos, and make it possible to
# deal with them in a manual fashion

# clone nsconf repos, create the namespaes without using ns-conf

# clone all infra repos used by the "$(ARGOCDINFRALAYER)" overlay from the argocd infra
# this way we could experiment with the repos locally, without using argocd

# ** local nsconf
# create all the namespaces in nsconf infra, but do it from the local machine, because the nsconf+argocd combo is flaky at the start
ns-conf-infra:
	git clone https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra.git

st/ns-conf-kludge: ns-conf-infra
	@IFS=$$'\n' namespaces=($$( yq e '.namespaces[].name'  ns-conf-infra/kustomize/overlays/$(ARGOCDINFRALAYER)/namespaces.yaml))
	for ns in "$${namespaces[@]}"; do
		echo "ns: $$ns"
		$(OC) create namespace $$ns
		$(OC) label namespaces  $$ns argocd.argoproj.io/managed-by=openshift-gitops
	done
	$(call st_done,$@)
# ** mess with infra repos
repo-urls.txt:
	grep -hoP 'repoURL:\s*\Khttps?://[^ ]*' argocd-infra/kustomize/clusters/$(ARGOCDINFRALAYER)/*|sort|uniq >repo-urls.txt
	cat repo-urls.txt

clone-infra-repos: repo-urls.txt
	@IFS=$$'\n' repos=($$(cat repo-urls.txt))
	mkdir $(ARGOCDINFRALAYER)-infra
	cd $(ARGOCDINFRALAYER)-infra
	for repo in "$${repos[@]}"; do
		echo "cloning $$repo"
		git clone $$repo
	done
# * Cluster autoscaling
st/deploy-autoscaler:
	@IFS=$$'\n' machinesets=($$($(OC) get machinesets --all-namespaces -o json|jq -r ".items[].metadata.name"))
	rm autoscale-machineset.yaml
	for machineset in "$${machinesets[@]}"; do
		export machineset
		echo "machineset: $$machineset"
		envsubst < autoscale-machineset-template.yaml >> autoscale-machineset.yaml
		echo "---" >> autoscale-machineset.yaml
	done
	cat  autoscale-machineset.yaml
	$(OC) apply -f autoscale.yaml
	$(OC) apply -f autoscale-machineset.yaml
	$(call st_done,$@)
